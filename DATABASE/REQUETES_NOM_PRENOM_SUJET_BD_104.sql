/*
	Toutes les colonnes
*/
SELECT * FROM t_pers_maillots_nba AS T1
INNER JOIN t_maillots_nba AS T2 ON T2.id_maillots_nba = T1.fk_maillots_nba
INNER JOIN t_personnes AS T3 ON T3.id_personne = T1.fk_personnes

/*
	Seulement certaines colonnes
*/
SELECT id_personne, nom_personne , id_casquettes_bonnets, equipe_casquettes_bonnets FROM t_pers_casquettes_bonnets AS T1
INNER JOIN t_casquettes_bonnets AS T2 ON T2.id_casquettes_bonnets = T1.fk_casquettes_bonnets
INNER JOIN t_personnes AS T3 ON T3.id_personne = T1.fk_personnes

/* 	
	Permet d'aficher toutes les lignes de la table de droite (t_genres) (qui est écrite en sql à droite de t_genres_films)
	y compris les lignes qui ne sont pas attribuées à des films.
*/
SELECT id_personne, prenom_personne , id_maillots_nfl, equipe_maillots_nfl FROM t_pers_maillots_nfl AS T1
INNER JOIN t_maillots_nfl AS T2 ON T2.id_maillots_nfl = T1.fk_maillots_nfl
RIGHT JOIN t_personnes AS T3 ON T3.id_personne = T1.fk_personnes

/* 	
	Permet d'aficher toutes les lignes de la table de droite (t_genres) (qui est écrite en sql à droite de t_genres_films)
	y compris les lignes qui ne sont pas attribuées à des films.
*/
SELECT id_personne, date_naissance_personne , id_shorts, equipe_shorts  FROM t_pers_shorts AS T1
RIGHT JOIN t_shorts AS T2 ON T2.id_shorts = T1.fk_shorts
LEFT JOIN t_personnes AS T3 ON T3.id_personne = T1.fk_personnes


/*
	Affiche TOUS les films qui n'ont pas de genre attribués
*/
SELECT id_personne, sexe_personne , id_sweat_capuche, equipe_sweat_capuche  FROM t_pers_sweat_capuche AS T1
RIGHT JOIN t_sweat_capuche AS T2 ON T2.id_sweat_capuche = T1.fk_sweat_capuche
LEFT JOIN t_personnes AS T3 ON T3.id_personne = T1.fk_personnes


/*
	Affiche SEULEMENT les films qui n'ont pas de genre attribués
*/

SELECT id_personne, nom_personne , id_tshirts, style_tshirts  FROM t_pers_tshirts AS T1
RIGHT JOIN t_tshirts AS T2 ON T2.id_tshirts = T1.fk_tshirts
LEFT JOIN t_personnes AS T3 ON T3.id_personne = T1.fk_personnes
WHERE T1.fk_personnes IS NULL