--

-- Database: NOM_PRENOM_SUJET_BD_104
-- Détection si une autre base de donnée du même nom existe

DROP DATABASE if exists FOUCART_YOANN_INFO1C;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS FOUCART_YOANN_INFO1C;

-- Utilisation de cette base de donnée

USE FOUCART_YOANN_INFO1C;


-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 10 Mars 2020 à 09:32
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `foucart_yoann_info1c`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_casquettes_bonnets`
--

CREATE TABLE `t_casquettes_bonnets` (
  `ID_Casquettes_Bonnets` int(11) NOT NULL,
  `Equipe_Casquettes_Bonnets` varchar(30) NOT NULL,
  `Type_Casquettes_Bonnets` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_casquettes_bonnets`
--

INSERT INTO `t_casquettes_bonnets` (`ID_Casquettes_Bonnets`, `Equipe_Casquettes_Bonnets`, `Type_Casquettes_Bonnets`) VALUES
(1, 'Boston Celtics', 'Casquette');

-- --------------------------------------------------------

--
-- Structure de la table `t_maillots_nba`
--

CREATE TABLE `t_maillots_nba` (
  `ID_Maillots_NBA` int(11) NOT NULL,
  `Equipe_Maillots_NBA` varchar(30) NOT NULL,
  `Joueur_Maillots_NBA` varchar(50) NOT NULL,
  `Numero_Maillots_NBA` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_maillots_nba`
--

INSERT INTO `t_maillots_nba` (`ID_Maillots_NBA`, `Equipe_Maillots_NBA`, `Joueur_Maillots_NBA`, `Numero_Maillota_NBA`) VALUES
(1, 'Boston Celtics', 'Jayson Tatum', '0');

-- --------------------------------------------------------

--
-- Structure de la table `t_maillots_nfl`
--

CREATE TABLE `t_maillots_nfl` (
  `ID_Maillots_NFL` int(11) NOT NULL,
  `Equipe_Maillots_NFL` varchar(30) NOT NULL,
  `Joueur_Maillots_NFL` varchar(30) NOT NULL,
  `Numero_Maillots_NFL` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_maillots_nfl`
--

INSERT INTO `t_maillots_nfl` (`ID_Maillots_NFL`, `Equipe_Maillots_NFL`, `Joueur_Maillots_NFL`, `Numero_Maillots_NFL`) VALUES
(1, 'Baltimore Ravens', 'Lamar Jackson', '8');

-- --------------------------------------------------------

--
-- Structure de la table `t_personnes`
--

CREATE TABLE `t_personnes` (
  `ID_Personne` int(11) NOT NULL,
  `Nom_Personne` varchar(50) NOT NULL,
  `Prenom_Personne` varchar(30) NOT NULL,
  `Date_naissance_Personne` date NOT NULL,
  `Sexe_Personne` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_personnes`
--

INSERT INTO `t_personnes` (`ID_Personne`, `Nom_Personne`, `Prenom_Personne`, `Date_naissance_Personne`, `Sexe_Personne`) VALUES
(1, 'Foucart', 'Yoann', '2003-12-23', 'Homme');

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_casquettes_bonnets`
--

CREATE TABLE `t_pers_casquettes_bonnets` (
  `ID_Pers_Casquettes_Bonnets` int(11) NOT NULL,
  `FK_Personnes` int(11) NOT NULL,
  `FK_Casquettes_Bonnets` int(11) NOT NULL,
  `date_Casquettes_Bonnets` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_maillots_nba`
--

CREATE TABLE `t_pers_maillots_nba` (
  `ID_Pers_Maillots_NBA` int(11) NOT NULL,
  `FK_Personnes` int(11) NOT NULL,
  `FK_Maillots_NBA` int(11) NOT NULL,
  `date_Maillots_NBA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_maillots_nfl`
--

CREATE TABLE `t_pers_maillots_nfl` (
  `ID_Pers_Maillots_NFL` int(11) NOT NULL,
  `FK_Personnes` int(11) NOT NULL,
  `FK_Maillots_NFL` int(11) NOT NULL,
  `date_Maillots_NFL` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_shorts`
--

CREATE TABLE `t_pers_shorts` (
  `ID_Pers_Shorts` int(11) NOT NULL,
  `FK_Personnes` int(11) NOT NULL,
  `FK_Shorts` int(11) NOT NULL,
  `date_Shorts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_sweat_capuche`
--

CREATE TABLE `t_pers_sweat_capuche` (
  `ID_Pers_Sweat_Capuche` int(11) NOT NULL,
  `FK_Personnes` int(11) NOT NULL,
  `FK_Sweat_Capuche` int(11) NOT NULL,
  `date_Sweat_Capuche` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_tshirts`
--

CREATE TABLE `t_pers_tshirts` (
  `ID_Pers_Tshirts` int(11) NOT NULL,
  `FK_Personnes` int(11) NOT NULL,
  `FK_Tshirts` int(11) NOT NULL,
  `date_Tshirts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_tshirts_manches_longues`
--

CREATE TABLE `t_pers_tshirts_manches_longues` (
  `ID_Pers_Tshirts_Manches_Longues` int(11) NOT NULL,
  `FK_Personnes` int(11) NOT NULL,
  `FK_Tshirts_Manches_Longues` int(11) NOT NULL,
  `date_Tshirts_Manches_Longues` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_vestes`
--

CREATE TABLE `t_pers_vestes` (
  `ID_Pers_Vestes` int(11) NOT NULL,
  `FK_Personnes` int(11) NOT NULL,
  `FK_Vestes` int(11) NOT NULL,
  `date_Vestes` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_shorts`
--

CREATE TABLE `t_shorts` (
  `ID_Shorts` int(11) NOT NULL,
  `Equipe_Shorts` varchar(30) NOT NULL,
  `Taille_Shorts` varchar(10) NOT NULL,
  `Annee_Shorts` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_shorts`
--

INSERT INTO `t_shorts` (`ID_Shorts`, `Equipe_Shorts`, `Taille_Shorts`, `Annee_Shorts`) VALUES
(1, 'Boston Celtics', 'L', '1982');

-- --------------------------------------------------------

--
-- Structure de la table `t_sweat_capuche`
--

CREATE TABLE `t_sweat_capuche` (
  `ID_Sweat_Capuche` int(11) NOT NULL,
  `Equipe_Sweat_Capuche` varchar(30) NOT NULL,
  `Style_Sweat_Capuche` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_sweat_capuche`
--

INSERT INTO `t_sweat_capuche` (`ID_Sweat_Capuche`, `Equipe_Sweat_Capuche`, `Style_Sweat_Capuche`) VALUES
(1, 'Boston Celtics', 'Hoody');

-- --------------------------------------------------------

--
-- Structure de la table `t_tshirts`
--

CREATE TABLE `t_tshirts` (
  `ID_Tshirts` int(11) NOT NULL,
  `Style_Tshirts` varchar(15) NOT NULL,
  `Marque_Tshirts` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_tshirts`
--

INSERT INTO `t_tshirts` (`ID_Tshirts`, `Style_Tshirts`, `Marque_Tshirts`) VALUES
(1, 'DRI-FIT', 'Nike');

-- --------------------------------------------------------

--
-- Structure de la table `t_tshirts_manches_longues`
--

CREATE TABLE `t_tshirts_manches_longues` (
  `ID_Tshirts_Manches_Longues` int(11) NOT NULL,
  `Equipe_Tshirts_Manches_Longues` varchar(30) NOT NULL,
  `Joueur_Tshirts_Manches_Longues` varchar(50) NOT NULL,
  `Numero_Tshirts_Manches_Longues` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_tshirts_manches_longues`
--

INSERT INTO `t_tshirts_manches_longues` (`ID_Tshirts_Manches_Longues`, `Equipe_Tshirts_Manches_Longues`, `Joueur_Tshirts_Manches_Longues`, `Numero_Tshirts_Manches_Longues`) VALUES
(1, 'Boston Celtics', 'Kemba Walker', '8');

-- --------------------------------------------------------

--
-- Structure de la table `t_vestes`
--

CREATE TABLE `t_vestes` (
  `ID_Vestes` int(11) NOT NULL,
  `Equipe_Vestes` varchar(30) NOT NULL,
  `Evenement_Vestes` varchar(20) NOT NULL,
  `Type_Vestes` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_vestes`
--

INSERT INTO `t_vestes` (`ID_Vestes`, `Equipe_Vestes`, `Evenement_Vestes`, `Type_Vestes`) VALUES
(1, 'Boston Celtics', 'All Star', 'Classics Jacket');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_casquettes_bonnets`
--
ALTER TABLE `t_casquettes_bonnets`
  ADD PRIMARY KEY (`ID_Casquettes_Bonnets`);

--
-- Index pour la table `t_maillots_nba`
--
ALTER TABLE `t_maillots_nba`
  ADD PRIMARY KEY (`ID_Maillots_NBA`);

--
-- Index pour la table `t_maillots_nfl`
--
ALTER TABLE `t_maillots_nfl`
  ADD PRIMARY KEY (`ID_Maillots_NFL`);

--
-- Index pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  ADD PRIMARY KEY (`ID_Personne`);

--
-- Index pour la table `t_pers_casquettes_bonnets`
--
ALTER TABLE `t_pers_casquettes_bonnets`
  ADD PRIMARY KEY (`ID_Pers_Casquettes_Bonnets`),
  ADD KEY `FK_Personnes` (`FK_Personnes`),
  ADD KEY `FK_Casquettes_Bonnets` (`FK_Casquettes_Bonnets`);

--
-- Index pour la table `t_pers_maillots_nba`
--
ALTER TABLE `t_pers_maillots_nba`
  ADD PRIMARY KEY (`ID_Pers_Maillots_NBA`),
  ADD KEY `FK_Personnes` (`FK_Personnes`),
  ADD KEY `FK_Maillots_NBA` (`FK_Maillots_NBA`);

--
-- Index pour la table `t_pers_maillots_nfl`
--
ALTER TABLE `t_pers_maillots_nfl`
  ADD PRIMARY KEY (`ID_Pers_Maillots_NFL`),
  ADD KEY `FK_Personnes` (`FK_Personnes`),
  ADD KEY `FK_Maillots_NFL` (`FK_Maillots_NFL`);

--
-- Index pour la table `t_pers_shorts`
--
ALTER TABLE `t_pers_shorts`
  ADD PRIMARY KEY (`ID_Pers_Shorts`),
  ADD KEY `FK_Personnes` (`FK_Personnes`),
  ADD KEY `FK_Shorts` (`FK_Shorts`);

--
-- Index pour la table `t_pers_sweat_capuche`
--
ALTER TABLE `t_pers_sweat_capuche`
  ADD PRIMARY KEY (`ID_Pers_Sweat_Capuche`),
  ADD KEY `FK_Personnes` (`FK_Personnes`),
  ADD KEY `FK_Sweat_Capuche` (`FK_Sweat_Capuche`);

--
-- Index pour la table `t_pers_tshirts`
--
ALTER TABLE `t_pers_tshirts`
  ADD PRIMARY KEY (`ID_Pers_Tshirts`),
  ADD KEY `FK_Personnes` (`FK_Personnes`),
  ADD KEY `FK_Tshirts` (`FK_Tshirts`);

--
-- Index pour la table `t_pers_tshirts_manches_longues`
--
ALTER TABLE `t_pers_tshirts_manches_longues`
  ADD PRIMARY KEY (`ID_Pers_Tshirts_Manches_Longues`),
  ADD KEY `FK_Personnes` (`FK_Personnes`),
  ADD KEY `FK_Tshirts_Manches_Longues` (`FK_Tshirts_Manches_Longues`);

--
-- Index pour la table `t_pers_vestes`
--
ALTER TABLE `t_pers_vestes`
  ADD PRIMARY KEY (`ID_Pers_Vestes`),
  ADD KEY `FK_Personnes` (`FK_Personnes`),
  ADD KEY `FK_Vestes` (`FK_Vestes`);

--
-- Index pour la table `t_shorts`
--
ALTER TABLE `t_shorts`
  ADD PRIMARY KEY (`ID_Shorts`);

--
-- Index pour la table `t_sweat_capuche`
--
ALTER TABLE `t_sweat_capuche`
  ADD PRIMARY KEY (`ID_Sweat_Capuche`);

--
-- Index pour la table `t_tshirts`
--
ALTER TABLE `t_tshirts`
  ADD PRIMARY KEY (`ID_Tshirts`);

--
-- Index pour la table `t_tshirts_manches_longues`
--
ALTER TABLE `t_tshirts_manches_longues`
  ADD PRIMARY KEY (`ID_Tshirts_Manches_Longues`);

--
-- Index pour la table `t_vestes`
--
ALTER TABLE `t_vestes`
  ADD PRIMARY KEY (`ID_Vestes`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_casquettes_bonnets`
--
ALTER TABLE `t_casquettes_bonnets`
  MODIFY `ID_Casquettes_Bonnets` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_maillots_nba`
--
ALTER TABLE `t_maillots_nba`
  MODIFY `ID_Maillots_NBA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_maillots_nfl`
--
ALTER TABLE `t_maillots_nfl`
  MODIFY `ID_Maillots_NFL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  MODIFY `ID_Personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_pers_casquettes_bonnets`
--
ALTER TABLE `t_pers_casquettes_bonnets`
  MODIFY `ID_Pers_Casquettes_Bonnets` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_pers_maillots_nba`
--
ALTER TABLE `t_pers_maillots_nba`
  MODIFY `ID_Pers_Maillots_NBA` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_pers_maillots_nfl`
--
ALTER TABLE `t_pers_maillots_nfl`
  MODIFY `ID_Pers_Maillots_NFL` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_pers_shorts`
--
ALTER TABLE `t_pers_shorts`
  MODIFY `ID_Pers_Shorts` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_pers_sweat_capuche`
--
ALTER TABLE `t_pers_sweat_capuche`
  MODIFY `ID_Pers_Sweat_Capuche` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_pers_tshirts`
--
ALTER TABLE `t_pers_tshirts`
  MODIFY `ID_Pers_Tshirts` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_pers_tshirts_manches_longues`
--
ALTER TABLE `t_pers_tshirts_manches_longues`
  MODIFY `ID_Pers_Tshirts_Manches_Longues` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_pers_vestes`
--
ALTER TABLE `t_pers_vestes`
  MODIFY `ID_Pers_Vestes` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_shorts`
--
ALTER TABLE `t_shorts`
  MODIFY `ID_Shorts` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_sweat_capuche`
--
ALTER TABLE `t_sweat_capuche`
  MODIFY `ID_Sweat_Capuche` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_tshirts`
--
ALTER TABLE `t_tshirts`
  MODIFY `ID_Tshirts` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_tshirts_manches_longues`
--
ALTER TABLE `t_tshirts_manches_longues`
  MODIFY `ID_Tshirts_Manches_Longues` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_vestes`
--
ALTER TABLE `t_vestes`
  MODIFY `ID_Vestes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_pers_casquettes_bonnets`
--
ALTER TABLE `t_pers_casquettes_bonnets`
  ADD CONSTRAINT `t_pers_casquettes_bonnets_ibfk_1` FOREIGN KEY (`FK_Personnes`) REFERENCES `t_personnes` (`ID_Personne`),
  ADD CONSTRAINT `t_pers_casquettes_bonnets_ibfk_2` FOREIGN KEY (`FK_Casquettes_Bonnets`) REFERENCES `t_casquettes_bonnets` (`ID_Casquettes_Bonnets`);

--
-- Contraintes pour la table `t_pers_maillots_nba`
--
ALTER TABLE `t_pers_maillots_nba`
  ADD CONSTRAINT `t_pers_maillots_nba_ibfk_1` FOREIGN KEY (`FK_Personnes`) REFERENCES `t_personnes` (`ID_Personne`),
  ADD CONSTRAINT `t_pers_maillots_nba_ibfk_2` FOREIGN KEY (`FK_Maillots_NBA`) REFERENCES `t_maillots_nba` (`ID_Maillots_NBA`);

--
-- Contraintes pour la table `t_pers_maillots_nfl`
--
ALTER TABLE `t_pers_maillots_nfl`
  ADD CONSTRAINT `t_pers_maillots_nfl_ibfk_1` FOREIGN KEY (`FK_Personnes`) REFERENCES `t_personnes` (`ID_Personne`),
  ADD CONSTRAINT `t_pers_maillots_nfl_ibfk_2` FOREIGN KEY (`FK_Maillots_NFL`) REFERENCES `t_maillots_nfl` (`ID_Maillots_NFL`);

--
-- Contraintes pour la table `t_pers_shorts`
--
ALTER TABLE `t_pers_shorts`
  ADD CONSTRAINT `t_pers_shorts_ibfk_1` FOREIGN KEY (`FK_Personnes`) REFERENCES `t_personnes` (`ID_Personne`),
  ADD CONSTRAINT `t_pers_shorts_ibfk_2` FOREIGN KEY (`FK_Shorts`) REFERENCES `t_shorts` (`ID_Shorts`);

--
-- Contraintes pour la table `t_pers_sweat_capuche`
--
ALTER TABLE `t_pers_sweat_capuche`
  ADD CONSTRAINT `t_pers_sweat_capuche_ibfk_1` FOREIGN KEY (`FK_Personnes`) REFERENCES `t_personnes` (`ID_Personne`),
  ADD CONSTRAINT `t_pers_sweat_capuche_ibfk_2` FOREIGN KEY (`FK_Sweat_Capuche`) REFERENCES `t_sweat_capuche` (`ID_Sweat_Capuche`);

--
-- Contraintes pour la table `t_pers_tshirts`
--
ALTER TABLE `t_pers_tshirts`
  ADD CONSTRAINT `t_pers_tshirts_ibfk_1` FOREIGN KEY (`FK_Personnes`) REFERENCES `t_personnes` (`ID_Personne`),
  ADD CONSTRAINT `t_pers_tshirts_ibfk_2` FOREIGN KEY (`FK_Tshirts`) REFERENCES `t_tshirts` (`ID_Tshirts`);

--
-- Contraintes pour la table `t_pers_tshirts_manches_longues`
--
ALTER TABLE `t_pers_tshirts_manches_longues`
  ADD CONSTRAINT `t_pers_tshirts_manches_longues_ibfk_1` FOREIGN KEY (`FK_Personnes`) REFERENCES `t_personnes` (`ID_Personne`),
  ADD CONSTRAINT `t_pers_tshirts_manches_longues_ibfk_2` FOREIGN KEY (`FK_Tshirts_Manches_Longues`) REFERENCES `t_tshirts_manches_longues` (`ID_Tshirts_Manches_Longues`);

--
-- Contraintes pour la table `t_pers_vestes`
--
ALTER TABLE `t_pers_vestes`
  ADD CONSTRAINT `t_pers_vestes_ibfk_1` FOREIGN KEY (`FK_Personnes`) REFERENCES `t_personnes` (`ID_Personne`),
  ADD CONSTRAINT `t_pers_vestes_ibfk_2` FOREIGN KEY (`FK_Vestes`) REFERENCES `t_vestes` (`ID_Vestes`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
